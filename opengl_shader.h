/**
 * @file		opengl_shader.h
 * @brief  	Convenince functions for working with OpenGL.
 */
#ifndef OPENGL_SHADER_H
#define OPENGL_SHADER_H

// NOTE: (sonictk) Headers are named differently on different OSes
#ifdef _WIN32
#include <gl/GL.h>
#elif __linux__ || __APPLE__
#include <GL/gl.h>
#endif


// NOTE: (sonictk) When these need to be more complicated data structures, they will be.
// Until then, they can just be IDs.
typedef GLuint GLVtxShader;
typedef GLuint GLFragShader;
typedef GLuint GLProgram;
typedef GLuint GLTexture;


/// This is the length of a OpenGL compiler error message.
globalVar const int kShaderCompileErrMsgMaxLen = 512;


/// Various statuses for the result of a OpenGL operation.
enum GLResult
{
	GLResult_Failure = INT_MIN,

	GLResult_CompileVertexShaderFailure,
	GLResult_CompileFragmentShaderFailure,
	GLResult_CreateShaderFailure,

	GLResult_ReadFailure,

	GLResult_LinkFailure,

	GLResult_InvalidProgram,
	GLResult_UnlinkedProgram,

	GLResult_Success = 0
};


/**
 * This function compiles the given shader ID and checks the result of the compilation.
 * This requires that an OpenGL context has been created first.
 *
 * @param id		The shader to compile.
 *
 * @return			The result of the compilation process.
 */
GLResult compileShaderAndCheckResult(GLuint id);


/**
 * This function compiles the given GLSL source code to a shader object.
 * This requires that an OpenGL context has been created first.
 *
 * @param shaderSource		The source code of the GLSL shader.
 * @param shaderType		The type of shader to compile.
 * @param shader			The reference that will be used for the newly-created
 * 						shader object.
 *
 * @return					The result of the compilation process.
 */
GLResult compileGLShader(const char *shaderSource,
						 GLenum shaderType,
						 GLuint &shader);


/**
 * This function compiles the given GLSL shader to a vertex shader object.
 * This requires that an OpenGL context has been created first.
 *
 * @param shaderFilePath	The file path to the GLSL shader. This must be
 * 						for a vertex shader.
 * @param shader			The reference that will be used for the newly-created
 * 						shader object.
 *
 * @return					The result of the compilation process.
 */
GLResult compileGLVtxShader(const char *shaderFilePath, GLVtxShader &shader);


/**
 * This function compiles the given GLSL shader to a fragment shader object.
 * This requires that an OpenGL context has been created first.
 *
 * @param shaderFilePath	The file path to the GLSL shader. This must be
 * 						for a fragment shader.
 * @param shader			The reference that will be used for the newly-created
 * 						shader object.
 *
 * @return					The result of the compilation process.
 */
GLResult compileGLFragShader(const char *shaderFilePath, GLFragShader &shader);


/**
 * This function compiles and links a OpenGL shader program from given vertex
 * and fragment shaders.
 * This requires that an OpenGL context has been created first.
 *
 * @param vtxShaderFilePath	The full file path to the GLSL vertex shader.
 * @param fragShaderFilePath	The full file path to the GLSL fragment shader.
 * @param program				The reference that will be used for the newly-created
 * 							shader program.
 *
 * @return						The result of the compilation/linking process.
 */
GLResult compileGLProgram(const char *vtxShaderFilePath,
						  const char *fragShaderFilePath,
						  GLProgram &program);


/**
 * Sets a uniform ``mat4`` to the values specified.
 *
 * @param program 			The GL program that contains the uniform variable.
 * @param uniformName 		The name of the uniform location to set.
 * @param matVals 			A 16-size array of ``float`` values to set.
 * @param transpose		Specifies if the matrix should be transposed before being set.
 *
 * @return 				The status code.
 */
GLResult setMat44Uniform(GLProgram program,
						 const char *uniformName,
						 const float *matVals,
						 bool transpose);


/**
 * Sets a uniform ``mat4`` to the matrix specified.
 *
 * @param program 			The GL program that contains the uniform variable.
 * @param uniformName 		The name of the uniform location to set.
 * @param mat 				The matrix to set.
 * @param transpose		Specifies if the matrix should be transposed before being set.
 *
 * @return 				The status code.
 */
GLResult setMat44Uniform(GLProgram program,
						 const char *uniformName,
						 const Mat44 mat,
						 bool transpose);


#endif /* OPENGL_SHADER_H */
