#include "opengl_shader.h"
#include <ssmath/ss_serialization.cpp>
#include <GL/glew.h>
#include <stdio.h>
#include <stdlib.h>


GLResult compileShaderAndCheckResult(GLuint id)
{
	glCompileShader(id);
	GLint sResult;
	glGetShaderiv(id, GL_COMPILE_STATUS, &sResult);
	if (!sResult) {
		char errMsg[kShaderCompileErrMsgMaxLen];
		glGetShaderInfoLog(id, kShaderCompileErrMsgMaxLen, NULL, errMsg);
		fprintf(stderr, "Shader compilation failed!\n%s\n", errMsg);
		return GLResult_Failure;
	}

	return GLResult_Success;
}


GLResult compileGLShader(const char *shaderSource,
						 GLenum shaderType,
						 GLuint &shader)
{
	shader = glCreateShader(shaderType);
	if (shader == 0) {
		perror("Error occurred when creating the shader object!\n");
		return GLResult_CreateShaderFailure;
	}

	glShaderSource(shader, 1, &shaderSource, NULL);
	GLResult compiled = compileShaderAndCheckResult(shader);
	if (compiled != GLResult_Success) {
		return GLResult_Failure;
	}

	return GLResult_Success;
}


GLResult compileGLVtxShader(const char *shaderFilePath, GLVtxShader &shader)
{
	ReadResult status;
	char *shaderSource = (char *)readText(shaderFilePath, status);
	if (status != ReadResult_Success) {
		return GLResult_ReadFailure;
	}

	GLResult compiled = compileGLShader(shaderSource, GL_VERTEX_SHADER, shader);
	if (compiled != GLResult_Success) {
		perror("Could not create vertex shader!\n");
		return GLResult_CompileVertexShaderFailure;
	}

	// NOTE: (sonictk) Once we've compiled the shader, no need for the source code
	free(shaderSource);

	return GLResult_Success;
}


GLResult compileGLFragShader(const char *shaderFilePath, GLFragShader &shader)
{
	ReadResult status;
	char *shaderSource = (char *)readText(shaderFilePath, status);
	if (status != ReadResult_Success) {
		return GLResult_ReadFailure;
	}

	GLResult compiled = compileGLShader(shaderSource, GL_FRAGMENT_SHADER, shader);
	if (compiled != GLResult_Success) {
		perror("Could not create fragment shader!\n");
		return GLResult_CompileVertexShaderFailure;
	}

	// NOTE: (sonictk) Once we've compiled the shader, no need for the source code
	free(shaderSource);

	return GLResult_Success;
}


GLResult compileGLProgram(const char *vtxShaderFilePath,
						  const char *fragShaderFilePath,
						  GLProgram &program)
{
	GLVtxShader vtxShader;
	GLResult result = compileGLVtxShader(vtxShaderFilePath, vtxShader);
	if (result != GLResult_Success) {
		return GLResult_Failure;
	}

	GLFragShader fragShader;
	result = compileGLFragShader(fragShaderFilePath, fragShader);
	if (result != GLResult_Success) {
		return GLResult_Failure;
	}

	program = glCreateProgram();
	glAttachShader(program, vtxShader);
	glAttachShader(program, fragShader);
	glLinkProgram(program);

	GLint sResult;
	glGetProgramiv(program, GL_LINK_STATUS, &sResult);
	if (!sResult) {
		char errMsg[kShaderCompileErrMsgMaxLen];
		glGetProgramInfoLog(program, kShaderCompileErrMsgMaxLen, NULL, errMsg);
		perror(errMsg);
		program = NULL; // NOTE: (sonictk) Clear it to avoid any funny business, also easier to check
		return GLResult_LinkFailure;
	}

	// NOTE: (sonictk) Once the program is linked, the shader objs are no longer needed
	glDeleteShader(vtxShader);
	glDeleteShader(fragShader);

	return GLResult_Success;
}


GLResult setMat44Uniform(GLProgram program,
						 const char *uniformName,
						 const float *matVals,
						 bool transpose)
{
	GLint location = glGetUniformLocation(program, uniformName);
	if (location == -1) {
		GLenum errNo = glGetError();
		switch (errNo) {
		case GL_INVALID_VALUE:
			return GLResult_InvalidProgram;
		case GL_INVALID_OPERATION:
			return GLResult_UnlinkedProgram;
		default:
			return GLResult_Failure;
		}
	}

	glUniformMatrix4fv(location, 1, (GLboolean)transpose, (GLfloat *)matVals);

	return GLResult_Success;
}


GLResult setMat44Uniform(GLProgram program,
						 const char *uniformName,
						 const Mat44 mat,
						 bool transpose)
{
	float vals[16];
	getValues(mat, vals, sizeof(vals));

	GLResult result = setMat44Uniform(program, uniformName, vals, transpose);

	return result;
}
