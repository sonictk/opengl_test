#version 330 compatibility

// Declare all the input/output vertex attributes
// NOTE: (sonictk) Layout/location qualifier is metadata so that the vertex attrs
// can be configured on the CPU.
layout (location = 0) in vec3 aPos;
//layout (location = 1) in vec3 aColor;
//layout (location = 2) in vec2 aUVCoord;
layout (location = 1) in vec2 aUVCoord;


// NOTE: (sonictk) If the types and names are the same in the vtx/frag shader,
// OpenGL will link those variables together when the shader program is linked
out vec3 vtxColor;
out vec2 uvCoord;

uniform mat4 transform;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;


void main()
{
	// gl_Position = transform * vec4(aPos.xyz, 1.0);
	// gl_Position = projection * view * model * vec4(aPos.xyz, 1.0);
	gl_Position = projection * view * model * transform * vec4(aPos.xyz, 1.0);

	// vtxColor = aColor;
	vtxColor = vec3(0.5, 0.3, 0.0);
	uvCoord = aUVCoord;
}
