#version 450 compatibility

uniform vec4 globalColor;

// NOTE: (sonictk) Default texture unit for a texture is 0
uniform sampler2D texSampler;
uniform sampler2D texSampler2;

// NOTE: (sonictk) this must be a vec4 since the shader needs to generate a final
// output colour; the first vec4 defined will be the output colour
out vec4 fragColor;

// NOTE: (sonictk) If the types and names are the same in the vtx/frag shader,
// OpenGL will link those variables together when the shader program is linked
in vec3 vtxColor;
in vec2 uvCoord;


void main()
{
	// fragColor = vec4(1.0, 1.0, 0.0, 1.0);
	//fragColor = vec4(vtxColor, 1.0) - globalColor;
	fragColor = mix(texture(texSampler, uvCoord),
					texture(texSampler2, uvCoord),
					0.5) * vec4(vtxColor, 1.0) - globalColor;
}
