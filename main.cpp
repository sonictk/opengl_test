#include <stdio.h>
#include <math.h>

// NOTE: (sonictk) GLEW requires that it be included first before GL.h, so we'll
// humour its vagaries
#define GLEW_STATIC
#include <GL/glew.h>

// NOTE: (sonictk) stb_image requires this define be set for memory allocation
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

// NOTE: (sonictk) Unity build setup
#include <ssmath/platform.h>
#include <ssmath/matrix_math.cpp>
#include "opengl_shader.cpp"

#include <GLFW/glfw3.h>


static int kWidth = 1280;
static int kHeight = 800;


int cleanup()
{
	fflush(stdout);
	fflush(stderr);

	// NOTE: (sonictk) This destroys all remaining windows and frees all resources,
	// along with setting the GLFW lib to an uninitialized state.
	glfwTerminate();

	return 0;
}


// NOTE: (sonictk) This callback function is called every time the main window is resized.
// It adjusts the viewport to match the new window size.
void windowResizeCB(GLFWwindow *window, int width, int height)
{
	glViewport(0, 0, width, height);
}


int processInput(GLFWwindow *window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	return 0;
}


int main(int argc, char *argv[])
{
	printf("Starting OpenGL test application...\n");

	GLint numOfVtxAttrsSupported;
	// NOTE: (sonictk) This is guaranteed to have at least 16 4-component vtx attrs available
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &numOfVtxAttrsSupported);

	printf("Num. of vertex attrs supported: %u\n", (unsigned int)numOfVtxAttrsSupported);

	// NOTE: (sonictk) Read the shader code from the source files
	char appPath[kMaxPathLen];
	int appPathLen = getAppPath(appPath, kMaxPathLen);
	if (appPathLen < 0) {
		perror("Error occurred when attempting to get the executable path!\n");
		return -1;
	}
	char dirPath[kMaxPathLen];
	int dirPathLen = getDirPath(appPath, dirPath);
	if (dirPathLen < 0) {
		perror("Could not determine the directory path!\n");
		return -1;
	}
	char vertShaderPath[kMaxPathLen];
	sprintf(vertShaderPath,
			"%s%c%s%c%s",
			dirPath,
			kPathDelimiter,
			"shaders",
			kPathDelimiter,
			"vert.glsl");
	printf("Reading vertex shader from: %s\n", vertShaderPath);

	char fragShaderPath[kMaxPathLen];
	sprintf(fragShaderPath,
			"%s%c%s%c%s",
			dirPath,
			kPathDelimiter,
			"shaders",
			kPathDelimiter,
			"frag.glsl");
	printf("Reading fragment shader from: %s\n", fragShaderPath);

	// NOTE: (sonictk) Load textures into memory
	char texPath[kMaxPathLen];
	sprintf(texPath,
			"%s%c%s%c%s",
			dirPath,
			kPathDelimiter,
			"data",
			kPathDelimiter,
			"container.jpg");
	printf("Reading texture from: %s\n", texPath);

	char tex2Path[kMaxPathLen];
	sprintf(tex2Path,
			"%s%c%s%c%s",
			dirPath,
			kPathDelimiter,
			"data",
			kPathDelimiter,
			"test_icon.png");
	printf("Reading texture from: %s\n", tex2Path);

	int imgWidth, imgHeight, imgNumOfChannels;
	// NOTE: (sonictk) This is because OpenGL expects 0.0y to be on the bottom
	stbi_set_flip_vertically_on_load(true);
	uint8 *imgData = (uint8 *)stbi_load(texPath,
										&imgWidth,
										&imgHeight,
										&imgNumOfChannels,
										STBI_rgb);
	if (!imgData) {
		perror("The 1st image could not be read!\n");
		return -1;
	}

	int img2Width, img2Height, img2NumOfChannels;
	uint8 *img2Data = (uint8 *)stbi_load(tex2Path,
										 &img2Width,
										 &img2Height,
										 &img2NumOfChannels,
										 STBI_rgb_alpha);
	if (!img2Data) {
		perror("The 2nd image could not be read!\n");
		return -1;
	}

	// NOTE: (sonictk) Enable as many extensions as possible, even on pre-release drivers
	glewExperimental = GL_TRUE;

	startPerfTimer();

	glfwInit();

	endPerfTimer();
	double profileTime = getPerfTimerValue();
	printf("GLFW init time: %f\n", profileTime);

	// NOTE: (sonictk) Uncomment this code to get Windowed fullscreen
	// GLFWmonitor *monitor = glfwGetPrimaryMonitor();
	// const GLFWvidmode *mode = glfwGetVideoMode(monitor);
	// glfwWindowHint(GLFW_RED_BITS, mode->redBits);
	// glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
	// glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
	// glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	GLFWwindow* window = glfwCreateWindow(kWidth, kHeight, "OpenGL test", NULL, NULL);
	// NOTE: (sonictk) More windowed fullscreen alternative code
	//GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "OpenGL test", monitor, NULL);
	if (!window) {
		perror("Failed to create window!\n");
		cleanup();

		return -1;
	}

	glfwMakeContextCurrent(window);

	GLenum result = glewInit();
	if (result != GLEW_OK) {
		fprintf(stderr, "Error occurred during GLEW init: %s\n", glewGetErrorString(result));
		cleanup();

		return -1;
	}

	glViewport(0, 0, kWidth, kHeight);
	glfwSetFramebufferSizeCallback(window, windowResizeCB);

	// NOTE: (sonictk) For drawing 3d, we need depth
	glEnable(GL_DEPTH_TEST);

	GLProgram shaderProgram;
	GLResult programCompiled = compileGLProgram(vertShaderPath,
													 fragShaderPath,
													 shaderProgram);
	if (programCompiled != GLResult_Success) {
		perror("Failed to create and link GL program!\n");
		return -1;
	}

	// NOTE: (sonictk) This needs to be in *normalized device coordinates* (i.e. from -1 to 1)
	// un-normalized coordinates will be clipped
	// float testVertices[] = {
	// 	// vtx positions 		colours			Uv Coords
	// 	+0.5f, +0.5f, 0.0f, 	1.0f, 0.0f, 0.0f, 	1.0f, 1.0f, //0 top right with red
	// 	+0.5f, -0.5f, 0.0f, 	0.0f, 1.0f, 0.0f, 	1.0f, 0.0f, //1 bottom right with green
	// 	-0.5f, -0.5f, 0.0f, 	1.0f, 0.0f, 1.0f, 	0.0f, 0.0f, //2 bottom left with purple
	// 	-0.5f, +0.5f, 0.0f, 	0.0f, 0.0f, 1.0f, 	0.0f, 1.0f, //3 top left with blue
	// 	+0.0f, +0.5f, 0.0f, 	1.0f, 1.0f, 1.0f, 	0.5f, 1.0f//4 top center with white
	// };
	// unsigned int vtxIdxs[] = {  // note that we start from 0!
	// 	0, 1, 3,   // first triangle
	// 	1, 2, 3    // second triangle
	// };

	// float uvCoords[] = {
	// 	0.0f, 0.0f, // lower left/origin
	// 	1.0f, 0.0f, // lower right
	// 	0.5f, 1.0f  // top center
	// };

	float testCubeData[] = {
		// vtx positions     // uv coords
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
		0.5f,	0.5f, -0.5f,  1.0f, 1.0f,
		0.5f,	0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f,	0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,	0.5f,  0.5f,  1.0f, 1.0f,
		0.5f,	0.5f,  0.5f,  1.0f, 1.0f,
		-0.5f,	0.5f,  0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

		-0.5f,	0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,	0.5f, -0.5f,  1.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f,	0.5f,  0.5f,  1.0f, 0.0f,

		0.5f,	0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,	0.5f, -0.5f,  1.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		0.5f,	0.5f,  0.5f,  1.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
		0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

		-0.5f,	0.5f, -0.5f,  0.0f, 1.0f,
		0.5f,	0.5f, -0.5f,  1.0f, 1.0f,
		0.5f,	0.5f,  0.5f,  1.0f, 0.0f,
		0.5f,	0.5f,  0.5f,  1.0f, 0.0f,
		-0.5f,	0.5f,  0.5f,  0.0f, 0.0f,
		-0.5f,	0.5f, -0.5f,  0.0f, 1.0f
	};

	Vec3 cubeInstancesPositions[] = {
		vec3( 0.0f,  0.0f,  0.0f),
		vec3( 2.0f,  5.0f, -15.0f),
		vec3(-1.5f, -2.2f, -2.5f),
		vec3(-3.8f, -2.0f, -12.3f),
		vec3( 2.4f, -0.4f, -3.5f),
		vec3(-1.7f,  3.0f, -7.5f),
		vec3( 1.3f, -2.0f, -2.5f),
		vec3( 1.5f,  2.0f, -2.5f),
		vec3( 1.5f,  0.2f, -1.5f),
		vec3(-1.3f,  1.0f, -1.5f)
	};

	// NOTE: (sonictk) If a texture runs over, use flat colour to visually indicate
	// mismatch in texture vs uv mapping
	float borderColor[] = {1.0f, 1.0f, 0.0f, 1.0f};

	// NOTE: (sonictk) Create the texture and bind it to the current context
	GLTexture texture, texture2;

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	// NOTE: (sonictk) Use nearest neighbour filtering for downscaling,
	// and bilinear filtering for upscaling textures
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// NOTE: (sonictk) Read the data from the image buffer and generate the mipmaps
	glTexImage2D(GL_TEXTURE_2D,
				 0,
				 GL_RGB,
				 imgWidth,
				 imgHeight,
				 0,
				 GL_RGB,
				 GL_UNSIGNED_BYTE, // NOTE: (sonictk)  The image is read in as uint8
				 imgData);
	glGenerateTextureMipmap(texture);

	glGenTextures(1, &texture2);
	glBindTexture(GL_TEXTURE_2D, texture2);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	glTexImage2D(GL_TEXTURE_2D,
				 0,
				 GL_RGBA,
				 imgWidth,
				 imgHeight,
				 0,
				 GL_RGBA,
				 GL_UNSIGNED_BYTE, // NOTE: (sonictk)  The image is read in as uint8
				 img2Data);

	glGenerateTextureMipmap(texture2);

	// NOTE: (sonictk) Now that the texture obj is created, no longer need the buffers
	stbi_image_free(imgData);
	stbi_image_free(img2Data);

	// NOTE: (sonictk) Create vertex buffer obj and bind to corresponding type
	GLuint vbo;
	glGenBuffers(1, &vbo);

	// NOTE: (sonictk) Create a vertex array obj and bind/configure it
	GLuint vao;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	// NOTE: (sonictk) Create an element buffer obj and bind it, also copy the indices
	// to it on the GPU; this is so we can reuse vtx data without duplication
	// GLuint ebo;
	// glGenBuffers(1, &ebo);
	// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
	// glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(vtxIdxs), vtxIdxs, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	// NOTE: (sonictk) Now allocate the memory and initialize it on the GPU by copying
	// the vertex array for OpenGL to use
	// GL_STATIC_DRAW implies to the GPU that the data will most likely not change at all

	//glBufferData(GL_ARRAY_BUFFER, sizeof(testVertices), testVertices, GL_STATIC_DRAW);

	glBufferData(GL_ARRAY_BUFFER, sizeof(testCubeData), testCubeData, GL_STATIC_DRAW);

	// NOTE: (sonictk) Index is set to 0 since that's the location configured in
	// the vertex shader; we specify how OpenGL should interpret the vertex data;
	// the VBO it takes it data from is the VBO currently bound to GL_ARRAY_BUFFER
	// NOTE: (sonictk) 8 floats per stride, and the first 3 are the vtx positions
	// glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), 0);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);

	// NOTE: (sonictk) Need to enable the attribute since it is disabled by default
	glEnableVertexAttribArray(0);

	// NOTE: (sonictk) 8 floats per stride, and the first 3 are the vtx pos,
	// then next 3 are vtx colours, then final 2 are UV coords
	// glVertexAttribPointer(1,
	// 					  3,
	// 					  GL_FLOAT,
	// 					  GL_FALSE,
	// 					  8 * sizeof(float),
	// 					  (const GLvoid *)(3 * sizeof(float)));
	// glEnableVertexAttribArray(1);

	// NOTE: (sonictk) Do the same for UVs
	// glVertexAttribPointer(2,
	// 					  2,
	// 					  GL_FLOAT,
	// 					  GL_FALSE,
	// 					  8 * sizeof(float),
	// 					  (const GLvoid *)(6 * sizeof(float)));
	// glEnableVertexAttribArray(2);

	glVertexAttribPointer(1,
						  2,
						  GL_FLOAT,
						  GL_FALSE,
						  5 * sizeof(float),
						  (const GLvoid *)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);

	// NOTE: (sonictk) Unbind the array buffer since the glVertexAttribPointer call
	// registered the VBO as the vtx attr's bound VBO already
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// NOTE: (sonictk) Unbind the VAO as well so that other VAO calls don't accidentally
	// modify this VAO
	glBindVertexArray(0);

	// NOTE: (sonictk) Also unbind the EBO responsible for the vtx idxs; we unbind it
	// after unbinding the VAO since the VAO also stores the glBindBuffer calls and
	// we don't want the VAO to have an unconfigured EBO.
	// glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// NOTE: (sonictk) install the shader program as part of current rendering state
	glUseProgram(shaderProgram);

	// NOTE: (sonictk) Set the uniform vars for the texture samplers
	glUniform1i(glGetUniformLocation(shaderProgram, "texSampler"), 0);
	glUniform1i(glGetUniformLocation(shaderProgram, "texSampler2"), 1);

	// NOTE: (sonictk) Find the location of the uniform var defined in the
	// frag. shader
	GLint globalColor = glGetUniformLocation(shaderProgram, "globalColor");

	// NOTE: (sonictk) Create matrices for 3D scene
	Mat44 modelMat = rotateBy(mat44(), vec3(1, 1, 0), -55.0f);
	// Mat44 viewWMat = translateBy(mat44(), vec3(0, 0, -3.5));

	// NOTE: (sonictk) Look at the origin with the camera in Z=3 (right-handed
	// space, camera looks in -Z) and Y-up vector
	Mat44 viewMat = view(vec3(0, 0, -3.5), vec3(0, 0, 0), vec3(0, 1, 0));

	Mat44 projMat = projection(45.0f, float(kWidth) / float(kHeight), 0.1f, 100.0f);

	// NOTE: (sonictk) Render loop
	while(!glfwWindowShouldClose(window)) {
		processInput(window);

		// NOTE: (sonictk) This is done for every render iteration since xform matrices
		// tend to change a lot over time

		// TODO: (sonictk) Figure out which of the matrix transformation functions are transposing incorrectly
		setMat44Uniform(shaderProgram, "model", modelMat, true);
		setMat44Uniform(shaderProgram, "view", viewMat, true);

		// TODO: (sonictk) Check what layout the projection matrix is being created in
		setMat44Uniform(shaderProgram, "projection", projMat, false);

		// NOTE: (sonictk) Test code to rotate the cube over time
		Mat44 transform = identityMat44();
		transform = rotateBy(transform, vec3(0.0f, 0.0f, (float)glfwGetTime() * 100));
		transform = scaleBy(transform, 0.5);

		setMat44Uniform(shaderProgram, "transform", transform, true);

		// note: (sonictk) Clear the screen at the start of each render iteration
		// otherwise we'll see ghosting from previous iterations
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		//glClearStencil(0);
		//glClearDepth(1.0f);
		glClear(GL_COLOR_BUFFER_BIT|GL_STENCIL_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

		// NOTE: (sonictk) Testing using uniform (global) shader vars
		// Updating a uniform var *requires* that the shader program be used first,
		// since it sets the uniform on the currently active shader program.
		double curTime = glfwGetTime();
		float greenVal = (sinf((float)curTime) / 2.0f) + 0.5f;
		glUniform4f(globalColor, 0.0f, greenVal, 0.0f, 1.0f);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, texture);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, texture2);

		glUseProgram(shaderProgram);

		// NOTE: (sonictk) When we want to draw, bind the VAO with the preferred settings
		// before drawing the object in case it was unbinded after drawing (in the
		// case of multiple VAOs)
		glBindVertexArray(vao);

		// NOTE: (sonictk) This enables drawing in wireframe
		//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		// NOTE: (sonictk) Draw using the index buffer; this takes the indices from the
		// EBO currently bound to the GL_ELEMENT_ARRAY_BUFFER target. The VAO also
		// keeps track of EBO bindings; the EBO that is currently bound while a VAO
		// is bound gets stored as a reference in the VAO. Binding to a VAO thus
		// automatically binds its EBO.
		// glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

		// NOTE: (sonictk) Drawing a couple of boxes for fun
		for (int i=0; i < 10; i++) {
			Mat44 tmpModelMat = mat44();
			tmpModelMat = translateBy(tmpModelMat, cubeInstancesPositions[i]);
			tmpModelMat = rotateBy(tmpModelMat, vec3(1.0f, 0.3f, 0.5f),  20.0f * i);

			setMat44Uniform(shaderProgram, "model", tmpModelMat, true);
			glDrawArrays(GL_TRIANGLES, 0, 36);
		}

		// glDrawArrays(GL_TRIANGLES, 0, 36);

		//glBindVertexArray(0); // NOTE: (sonictk) There's only one VAO so no need to unbind it every time

		// NOTE: (sonictk) Draw the object (non-EBO mode)
		//glDrawArrays(GL_TRIANGLES, 0, 3);

		// NOTE: (sonictk) Swap the front/back colour buffers and show as output to screen.
		// (GLFW is using double-buffering to avoid flickering)
		glfwSwapBuffers(window);

		// NOTE: (sonictk) Checks if any events have been triggered and calls the
		// callback functions, along with updating the window state.
		glfwPollEvents();
	}

	// NOTE: (sonictk) Deallocate the vertex buffer/array objs once drawing is done
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	// glDeleteBuffers(1, &ebo);
	glDeleteTextures(1, &texture);
	glDeleteTextures(1, &texture2);

	cleanup();

	return 0;
}
